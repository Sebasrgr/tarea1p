import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _counter = 0;
  bool _color = false;
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        title: Text('Click the FAB'),
        centerTitle: true,
        textTheme: Typography.dense2018,
        backgroundColor: Colors.indigo,
        actions: [
          IconButton(
            icon: Icon(Icons.thumb_up),
            onPressed: () {
              _color = !_color;
              setState(() {});
            },
            color: _color ? Colors.indigoAccent[100] : Colors.white,
          ),
        ],
      ),
      body: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Clicks: $_counter",
            style: TextStyle(
              fontSize: 24,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      )),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _scaffoldkey.currentState.hideCurrentSnackBar();
          _counter++;
          if (_counter % 2 == 0) {
            print("Even");
            var snackBar = SnackBar(
              content: Text("Even"),
              behavior: SnackBarBehavior.floating,
            );
            _scaffoldkey.currentState.showSnackBar(snackBar);
          } else {
            print("Odd");
            var snackBarAction = SnackBar(
                content: Text("Odd number"),
                behavior: SnackBarBehavior.floating,
                action: SnackBarAction(
                    label: "FECHA",
                    onPressed: () {
                      showDialog(
                          context: context,
                          child: SimpleDialog(
                            title: Text(
                              "Date and Time",
                            ),
                            children: [
                              Text(
                                DateTime.now().toString(),
                                textAlign: TextAlign.center,
                              ),
                              Column(
                                children: [
                                  SizedBox(
                                    height: 24,
                                  ),
                                  RaisedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("Close"),
                                    color: Colors.white,
                                    textColor: Colors.black87,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                  )
                                ],
                              )
                            ],
                          ));
                    }));
            _scaffoldkey.currentState.showSnackBar(snackBarAction);
          }
          setState(() {});
        },
        backgroundColor: Colors.black87,
        foregroundColor: Colors.white,
        isExtended: true,
        icon: Icon(Icons.add),
        label: Text("CLICK ME"),
      ),
    );
  }
}
